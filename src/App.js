import "./App.css";
import ExTryGlasses from "./Try_Glasses_App_Online/ExTryGlasses";

function App() {
  return (
    <div className="App">
      <ExTryGlasses />
    </div>
  );
}

export default App;
