import React, { Component } from "react";
import styles from "./glasses.module.css";

export default class ItemGlasses extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className={`${styles.glasses} col-3`}>
        <img
          onClick={() => {
            this.props.handleClick(this.props.data);
          }}
          className={`${styles.img_Glasses}`}
          src={url}
          alt="glasses"
        />
      </div>
    );
  }
}
