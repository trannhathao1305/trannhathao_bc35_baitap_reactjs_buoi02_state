import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import Display from "./Display";
import ItemGlasses from "./ItemGlasses";
import styles from "./glasses.module.css";

export default class ExTryGlasses extends Component {
  state = {
    glassesArr: dataGlasses,
    detail: dataGlasses[0],
  };
  handleClickTryGlasses = (glasses) => {
    this.setState({ detail: glasses });
  };
  renderListGlasses = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
          handleClick={this.handleClickTryGlasses}
          data={item}
          key={index}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <Display detail={this.state.detail} />
        <div className={`${styles.cont_glasses} row`}>
          {this.renderListGlasses()}
        </div>
      </div>
    );
  }
}
