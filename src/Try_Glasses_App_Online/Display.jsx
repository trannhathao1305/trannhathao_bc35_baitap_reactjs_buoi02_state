import React, { Component } from "react";
import styles from "./glasses.module.css";

export default class Display extends Component {
  render() {
    let { url, name, desc } = this.props.detail;
    return (
      <div>
        <img
          className={`${styles.imgBackground}`}
          src="./ExGlasses/glassesImage/background.jpg"
          alt=""
        />
        <h2 className={`${styles.title}`}>
          TRY GLASSES APP ONLINE <span>VIP</span>
        </h2>
        <div className={`${styles.img_Model}`}>
          <div className={`${styles.Model_Left}`}>
            <img
              className={`${styles.img_Model_Left}`}
              src="./ExGlasses/glassesImage/model.jpg"
              alt="model"
            />
            <img
              style={{
                width: "140px",
                height: "50px",
                position: "absolute",
                left: "56px",
                top: "75px",
                opacity: "0.8",
              }}
              src={url}
              alt=""
            />
            <div className={`${styles.detail_Name_Desc}`}>
              <h5>{name}</h5>
              <p>{desc}</p>
            </div>
          </div>
          <span>
            <i className="fa-solid fa-arrows-left-right" />
          </span>
          <div className={`${styles.Model_Right}`}>
            <img
              className={`${styles.img_Model_Right}`}
              src="./ExGlasses/glassesImage/model.jpg"
              alt="model"
            />
          </div>
        </div>
      </div>
    );
  }
}
